-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: lottery
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bank_money`
--

DROP TABLE IF EXISTS `bank_money`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank_money` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `balance` int(12) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank_money`
--

LOCK TABLES `bank_money` WRITE;
/*!40000 ALTER TABLE `bank_money` DISABLE KEYS */;
INSERT INTO `bank_money` VALUES (1,78133);
/*!40000 ALTER TABLE `bank_money` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank_prizes`
--

DROP TABLE IF EXISTS `bank_prizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank_prizes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prize_name` varchar(50) NOT NULL,
  `prize_count` int(12) DEFAULT '0',
  `picture` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank_prizes`
--

LOCK TABLES `bank_prizes` WRITE;
/*!40000 ALTER TABLE `bank_prizes` DISABLE KEYS */;
INSERT INTO `bank_prizes` VALUES (1,'car',-131,NULL),(2,'boat',-6,NULL),(3,'TV',0,NULL),(4,'Playstation',3,NULL);
/*!40000 ALTER TABLE `bank_prizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prize_winners`
--

DROP TABLE IF EXISTS `prize_winners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prize_winners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `prize_name` varchar(50) DEFAULT NULL,
  `id_prize` int(12) DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  `is_money` tinyint(4) DEFAULT NULL,
  `is_sending` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prize_winners`
--

LOCK TABLES `prize_winners` WRITE;
/*!40000 ALTER TABLE `prize_winners` DISABLE KEYS */;
INSERT INTO `prize_winners` VALUES (1,4,'Playstation',4,2,NULL,0),(2,4,'boat',2,11,NULL,0),(3,4,'car',1,140,NULL,0),(4,4,'TV',3,8,NULL,0),(5,4,NULL,NULL,21869,1,0);
/*!40000 ALTER TABLE `prize_winners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `balance` int(20) DEFAULT '0',
  `points` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (4,'spocky','256@flant.ru','9188db2f8450d474294bf7a6421fa0ea',0,2891);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-08 14:27:38
