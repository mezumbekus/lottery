<?php
return [
	'register' => 'user/register',
	'login' => 'user/login',
	'logout' => 'user/logout',
	'cabinet' => 'cabinet/index',
	'play/([0-9]+)' => 'game/play/$1',
	
	'' => 'site/index'
];

