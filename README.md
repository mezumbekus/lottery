# Прототип приложения для розыгрыша призов


Данное приложение написано на php 7 . Использован MVC паттерн в качестве каркаса приложения.

База данных MySQL, можно поменять на другую в ```/components/db.php```

Настройки доступа к базе находятся в ```/config/db_params.php```

Чтобы запустить данное приложение, необходимо прописать в конфиге веб-сервера, чтобы все запросы шли на index.php

Весь исходник написан на нативном PHP и JS.

Пример конфига для nginx:

```

server {
    charset utf-8;
    client_max_body_size 128M;

    listen 5080; ## listen for ipv4
    #listen [::]:80 default_server ipv6only=on; ## listen for ipv6

    server_name lottery.local;
    root        /var/www/lottery;
    index       index.php;

    access_log  /var/log/nginx/access.log;
    error_log   /var/log/nginx/lottery_error.log;

    location / {
        # Redirect everything that isn't a real file to index.php
        try_files $uri $uri/ /index.php$is_args$args;
    }

    # uncomment to avoid processing of calls to non-existing static files by Yii
    #location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$ {
    #    try_files $uri =404;
    #}
    #error_page 404 /404.html;

    # deny accessing php files for the /assets directory
    location ~ ^/assets/.*\.php$ {
        deny all;
    }

    location ~ \.php$ {
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        #fastcgi_pass 127.0.0.1:9000;
        fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
        try_files $uri =404;
    }

    location ~* /\. {
        deny all;
    }
}
```
Также необходимо создать базу и таблицы, пример дампа прилагается.


