<?php include ROOT.'/views/header.php'; ?>

<div class="container">
	<h2>Вход пользователя</h2>
	<form action="#" method="post">
		<input type="text" name="name" placeholder="Имя для входа в аккаунт" class="field">
		<input type="password" name="password" placeholder="Пароль" class="pass1 field">
		<input type="submit" name="submit" value="Готово!">
	</form>
	<a href="register">Регистрация</a>
	<?php if(count($errors)){
		foreach ($errors as $v) {
			echo "<b>---{$v}---</b><br>";
		}
	}?>
</div>

<?php include ROOT.'/views/footer.php'; ?>