<?php include ROOT.'/views/header.php'; ?>

<div class="container">
	<div class="user-id" hidden data-userid="<?php echo $data['id']; ?>"></div>
	<h2>Добро пожаловать <?php echo $data['name']; ?></h2>
	<h4>Ваш баланс: <span class="main__balance"><?php echo $data['balance']; ?></span></h4>
	<h4>Выигрышные деньги: <span class="main__winmoney"><?php echo $winmoney; ?></span></h4>
	<h4>Баллы: <span class="main__points"><?php echo $data['points']; ?></span></h4>
	<a href="cabinet">Личный кабинет</a>
	<a href="logout">Выйти</a><br>
	<a href="play" class="play-button">Играть</a>
	<div class="game-area">
		
	</div>
	<script type="text/javascript" src="/views/js/common.js"></script>
</div>

<?php include ROOT.'/views/footer.php'; ?>
