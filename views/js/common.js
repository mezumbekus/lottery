var btn = document.querySelector('.play-button');
var iduser = document.querySelector('.user-id');
var zone = document.querySelector('.game-area');
var balance = document.querySelector('.main__balance');
var winmoney = document.querySelector('.main__winmoney');

var points = document.querySelector('.main__points');
btn.onclick = function(e){
	
	e.preventDefault();
	var xhr = new XMLHttpRequest();
	
	xhr.open('GET','play/'+(+iduser.getAttribute('data-userid')));
	xhr.send();

	xhr.onreadystatechange = function(){
		if(this.readyState != 4) return;
		if(this.status != 200){
			console.dir(this);			
			return;
		}
		
		var parsed = JSON.parse(xhr.responseText);

		for(var k in parsed){
			if(parsed['type'] == 2){
				zone.innerHTML = parsed['name'] + ' ' + parsed['prize']['name'];
				continue;
			}else {

				zone.innerHTML = parsed['name'];
				switch(parsed['type']){
					case 0: 

						winmoney.innerHTML = +winmoney.innerHTML + (+parsed['count']);
						return;
					case 1:
						points.innerHTML = +points.innerHTML + (+parsed['count']);
						return;
				}
			}
			
		
		}
	}


}
