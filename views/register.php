<?php include ROOT.'/views/header.php'; ?>

<div class="container">
<?php if($regok === false): ?>
	<h2>Регистрация пользователя</h2>
	<form action="#" method="post">
		<input type="text" name="name" placeholder="Имя для входа в аккаунт" class="field">
		<input type="text" name="email" placeholder="email" class="field">
		<input type="password" name="password" placeholder="Пароль" class="pass1 field">
		<input type="password" name="password" placeholder="Повторите пароль" class="pass2 field">
		<input type="submit" name="submit" value="Готово!">
	</form>
	<?php if(count($errors)){
		foreach ($errors as $v) {
			echo "<b>---{$v}---</b><br>";
		}
	}?>
<?php else: ?>
	<h2>Пользователь зарегистрирован!</h2>
	<a href="/">На главную</a>
<?php endif; ?>
</div>

<?php include ROOT.'/views/footer.php'; ?>