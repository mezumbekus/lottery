<?php 

class Game {

	const PRIZE_MONEY = 0;
	const PRIZE_POINTS = 1;
	const PRIZE_GOODS = 2;
	const PRIZE_NOTHING = 3;


	const POINTS_CNT = 50;

	private static function getRandomVal($len,$sub = 0){
		return mt_rand(0,999)%$len+$sub; 
	}



	private static function getGoods($userid){


		$sql = 'SELECT count(*) as els FROM bank_prizes WHERE prize_count > 0';
		$sql1 = 'SELECT id,prize_name,picture FROM bank_prizes';
		$sql2 = 'UPDATE bank_prizes SET prize_count = prize_count - 1 WHERE id = :prize_id';
		$sql3 = 'UPDATE prize_winners SET count = count + 1 WHERE id_user=:id_user AND id_prize = :id_prize';
		$sql4 = 'INSERT INTO prize_winners (id_user,prize_name,id_prize,count) VALUES (:id_user,:prize_name,:id_prize,1)';
		$sql5 = 'SELECT COUNT(*) as count FROM prize_winners WHERE id_user=:id_user AND id_prize=:id_prize';
		$db = Db::getConnection();
		
		$result = $db->query($sql);
		$lines = $result->fetchAll()[0]['els'];
		if($lines == 0)
			return false;
		$result = $db->query($sql1);
		$prizes = $result->fetchAll(PDO::FETCH_BOTH);
		
		$winnumber = self::getRandomVal($lines);
		$winprize = $prizes[$winnumber]['id'];
		

		$db->beginTransaction();
		$stm = $db->prepare($sql2);
		$stm->bindParam(':prize_id',$winprize,PDO::PARAM_INT);
		$result = $stm->execute();
		if($result)
			$db->commit();
		else
			$db->rollBack();

		
		$stm = $db->prepare($sql5);
		$stm->bindParam(':id_user',$userid,PDO::PARAM_INT);
		$stm->bindParam(':id_prize',$winprize,PDO::PARAM_INT);
		$result = $stm->execute();
		$count = $stm->fetchAll()[0]['count'];
		if($count>0){
			$stm = $db->prepare($sql3);
			$stm->bindParam(':id_user',$userid,PDO::PARAM_INT);
			$stm->bindParam(':id_prize',$winprize,PDO::PARAM_INT);
		}else{

			$stm = $db->prepare($sql4);
			$stm->bindParam(':id_user',$userid,PDO::PARAM_INT);
			$stm->bindParam(':id_prize',$winprize,PDO::PARAM_INT);
			$stm->bindParam(':prize_name',$prizes[$winnumber]['prize_name'],PDO::PARAM_STR);
		}
		if($stm->execute())
			return $prizes[$winnumber];
		else return false;
	}
	
	
	private static function getMoney($userid){
				
				$sql = 'SELECT * FROM bank_money';
				$sql2 = 'UPDATE bank_money SET balance = :count WHERE id = 1';
				$sql3 = 'UPDATE prize_winners SET count = count + :count WHERE id_user=:id_user AND is_money = 1';
				$sql4 = 'INSERT INTO prize_winners (id_user,count,is_money,prize_name) VALUES (:id_user,:count,1,NULL)';
				$sql5 = 'SELECT count(*) as count FROM prize_winners WHERE id_user = :id_user AND is_money = 1';
				$db = Db::getConnection();
				$db->beginTransaction();
				$result = $db->query($sql);
				$balance = $result->fetchAll()[0]['balance'];
				if($balance > 0){
					$cur_val = self::getRandomVal($balance,1);
					$stm = $db->prepare($sql2);
					$diff = ($balance-$cur_val);
					$stm->bindParam(':count',$diff,PDO::PARAM_INT);
					$result = $stm->execute();
					if($result){
				 		$db->commit();
				 		}
					else{
					 	$db->rollBack();
					 	return false;
				 		}
				}else{
					return false;
				}
				$stm = $db->prepare($sql5);
				$stm->bindParam(':id_user',$userid,PDO::PARAM_INT);
				$stm->execute();
				$exists = $stm->fetchAll()[0]['count'];
				if($exists > 0){
					$stm = $db->prepare($sql3);
					
				}else{
					$stm = $db->prepare($sql4);
				}
				$stm->bindParam(':id_user',$userid,PDO::PARAM_INT);
				$stm->bindParam(':count',$cur_val,PDO::PARAM_INT);

				if($stm->execute())
					return $cur_val;
				return false;
				
	}

	private static function getPoints($points){
				
				$db = Db::getConnection();
				$sql = 'UPDATE users SET points = points + :points';
				$stm = $db->prepare($sql);
				$points = self::getRandomVal($points);
				$stm->bindParam(':points',$points,PDO::PARAM_INT);
				if($stm->execute())
					return $points;
				return false;
	}



	public static function getRandomPrize($userid){
		
		$typeOfPrize = self::getRandomVal(4);
		$prize = ['type' => $typeOfPrize,'prize' == null];

		switch($typeOfPrize){

			case self::PRIZE_MONEY:
				$winmoney = self::getMoney($userid);
				$prize['name'] = 'Вы выиграли деньги! в количестве: '.$winmoney.' euro';
				$prize['count'] = $winmoney;

				break;

			case self::PRIZE_POINTS:
				$points = self::getPoints(self::POINTS_CNT);
				$prize['name'] = 'Вы выиграли баллы в количестве: ' . $points;
				$prize['count'] = $points;
				break;

			case self::PRIZE_GOODS:
				$data = self::getGoods($userid);
				$prize['name'] = 'Вы выиграли подарок!';
				$prize['prize'] = 	[
										'name' => $data['prize_name'],
										'prize_id' => $data['id'],
										'pic' => $data['picture'] 
									];
				break;
			case self::PRIZE_NOTHING:
				$prize['name'] = 'К сожалению вы ничего не выиграли( Попытайте еще раз удачу!';
				break;
		}
		unset($prize[0]);
		return $prize;

	}

}