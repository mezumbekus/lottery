<?php 

class User
{
	const USER_CHECKTYPE_LOGIN = 0;
	const USER_CHECKTYPE_PASSWORD = 1;
	const USER_CHECKTYPE_EMAIL = 2;

	public static function checkString($name,$type){
		switch($type){

		case self::USER_CHECKTYPE_LOGIN:	
			if(strlen($name)<6 || preg_match("/[^(\w)|(\s)]/", $name)){
				return false;
			}
			return true;
		case self::USER_CHECKTYPE_PASSWORD:
			if(strlen($name)<8 || preg_match("/[^(\w)|(\s)]/", $name)){
				return false;
			}
				return true;
		case self::USER_CHECKTYPE_EMAIL:
			if (strlen($name)<3 || preg_match("/[0-9a-z]+@[a-z]+\.[a-z]+/",$name) != 1){
				return false;
			}
				return true;
		}
		
	}


	public static function checkLoginExists($name){
		$db = Db::getConnection();
		$sql = "SELECT count(name) from users where name = :name";
		$result = $db->prepare($sql);
		$result->bindParam(':name',$name,PDO::PARAM_STR);
		$result->execute();
		if($result->fetchColumn())
			return true;
		return false;

	}

	public static function registerUser($name,$email,$pass){
		
		$db = Db::getConnection();
		
		$sql = "INSERT INTO users (name,email,password) VALUES (:name,:email,:password)";
		$pass = md5($pass);
		$result = $db->prepare($sql);
		$result->bindParam(':name',$name,PDO::PARAM_STR);
		$result->bindParam(':email',$email,PDO::PARAM_STR);
		$result->bindParam(':password',$pass,PDO::PARAM_STR);
		if($result->execute())
			return $db->lastInsertId();
		return false;
	}

	public static function checkUserData($name,$pass){
		
		$db = Db::getConnection();

		$sql = "SELECT id FROM users WHERE name = :name AND password = :pass";

		$result = $db->prepare($sql);
		$result->bindParam(':name',$name,PDO::PARAM_STR);
		$pass = md5($pass);
		$result->bindParam(':pass',$pass);
		$result->execute();
		$user = $result->fetch();
		if($user)
			return $user['id'];
		return false;
	}

	public static function getNameById($id){
		$db = Db::getConnection();

		$sql = 'SELECT name FROM users WHERE id = :id';

		$result = $db->prepare($sql);
		$result->bindParam(':id',$id);
		$result->execute();
		$name = $result->fetch();
		if($name)
			return $name['name'];
		return false;


	}

	public static function getUserDataById($id){
		$db = Db::getConnection();

		$sql = 'SELECT  u.id,u.name,u.balance,u.points,p.prize_name,p.count FROM users as u LEFT JOIN prize_winners AS p ON u.id = p.id_user WHERE u.id = :id';

		$result = $db->prepare($sql);
		$result->bindParam(':id',$id);
		$result->execute();
		$data = $result->fetch();
		return $data;

	}

	public static function getWinMoney($id){

		$db = Db::getConnection();

		$sql = 'SELECT count FROM prize_winners WHERE is_money = 1 AND id_user = :id';
		$result = $db->prepare($sql);
		$result->bindParam(':id',$id);
		$result->execute();
		return $result->fetch()['count'];
		

	}

	public static function checkAuth(){
		if(!isset($_SESSION['userid'])){
				header('Location: login');
		}
	}

}