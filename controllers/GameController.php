<?php  

class GameController 
{
	public function actionPlay($id){
		header("Content-type: application/json; charset=utf-8");
		$prize = Game::getRandomPrize($id);

		echo json_encode($prize);
		return true;
	}
}