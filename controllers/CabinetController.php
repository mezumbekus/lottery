<?php 

class CabinetController {

	public function actionIndex(){
		User::checkAuth();
		$data = User::getUserDataById($_SESSION['userid']);
		$winmoney = User::getWinMoney($_SESSION['userid']);
		
		include ROOT.'/views/cabinet.php';

		return true;
	}
}