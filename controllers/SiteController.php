<?php

	class SiteController 
	{
		public function actionIndex(){
			
			User::checkAuth();
			
			$data = User::getUserDatabyId($_SESSION['userid']);
			$winmoney = User::getWinMoney($_SESSION['userid']);
			include ROOT.'/views/main.php';
			return true;
		}
	}