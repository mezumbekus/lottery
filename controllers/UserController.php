<?php 
class UserController 
{
	public function actionRegister(){
		$errors = [];
		$regok = false;
		if(isset($_POST['submit'])){
			$name = $_POST['name'];
			if(!User::checkString($name,USER::USER_CHECKTYPE_LOGIN))
				$errors[]='Недопустимое имя, длина должна быть больше 6 символов и содержать только буквы латиницы  и цифры';
			else{
				if(User::checkLoginExists($name))
					$errors[] = 'Такой логин уже существует';
			}

			$email = $_POST['email'];
			if(!User::checkString($email,USER::USER_CHECKTYPE_EMAIL))
				$errors[]='Недопустимый email!';


			
			$password = $_POST['password'];

			if(!User::checkString($password,USER::USER_CHECKTYPE_PASSWORD))
				$errors[]='Пароль должен быть больше 8 симовлов и состоять из букв латиницы и(или) цифр';
			if(!count($errors)){
				$regok = User::registerUser($name,$email,$password);
				if($regok!== false){
					$_SESSION['userid'] = $regok;
				}
				else
					$errors[] = 'Ошибка регистрации';
			}


		}
		
		require_once ROOT.'/views/register.php';
		return true;
	}

	public function actionLogin(){
		$errors = [];
		if(isset($_SESSION['userid']))
			header('Location: logout');
		if(isset($_POST['submit'])){
			$name = $_POST['name'];
			if(!User::checkString($name,USER::USER_CHECKTYPE_LOGIN))
				$errors[] = 'Неправильный логин';
			$pass = $_POST['password'];
			$userid = User::checkUserData($name,$pass);
			if($userid === false)
				$errors[] = 'Неправильный логин или пароль';
			else{
				$_SESSION['userid'] = $userid;
				header('Location: /');
			}

		}
		include ROOT.'/views/login.php';
		
		return true;
	}

	public function actionLogout(){
		if(isset($_POST['submit'])){
			unset($_SESSION['userid']);
			header('Location: login');
		}
		$name = User::getNameById($_SESSION['userid']);
		include ROOT.'/views/logout.php';
		return true;
	}
	

}