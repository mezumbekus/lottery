<?php

class Router 
{

	private $routes;
	
	public function __construct()
	{
		$routesPath = ROOT.'/config/routes.php';
		$this->routes = include($routesPath);
	}
	
	
	public function run()
	{
		if(!empty($_SERVER['REQUEST_URI'])){
		   $uri = trim($_SERVER['REQUEST_URI'],'/');
		}
		foreach($this->routes as $uriPattern => $path){
		   if(preg_match("~$uriPattern~",$uri)){
			$internalPath = preg_replace("~$uriPattern~",$path,$uri);
			$segments = explode('/',$internalPath);
			$controllerName = ucfirst(array_shift($segments).'Controller');
			$actionName = 'action'.ucfirst(array_shift($segments));
			$controllerFile = ROOT . '/controllers/' . $controllerName . '.php';
			if(file_exists($controllerFile))
			    include_once($controllerFile);
			else die;
			$controllerObject = new $controllerName();
			$result = call_user_func_array([$controllerObject,$actionName],$segments);
			if($result != null)
				break;

		    }
		}
	}

}


