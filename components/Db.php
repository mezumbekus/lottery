<?php

Class Db
{
	public static $db = null;
	
	public static function getConnection()
	{
		

		$conf = include(ROOT.'/config/db_params.php');
		$dsn = "mysql:host={$conf['host']};dbname={$conf['dbname']};port={$conf['port']}";
		if(!self::$db){
			try
			{
			self::$db = new PDO($dsn,$conf['user'],$conf['pass'],
					[
				    	PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				    	PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
			        ]);
			}catch(PDOException $e){
		 		die($e->getMessage());
			}
		}
		return self::$db;
	}
}

